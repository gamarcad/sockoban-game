package observer;


/**
 * The interface Observer {@code Observer} is used to implement the observer - observable pattern.
 * This interface is used to observe another class and to update the class that
 * implements Observer as a result of a change in the observable state
 *
 * @see Observable
 */
public interface Observer {

    /**
     * Triggered when the state of one of the observable that the observer class is observing change
     *
     * @see Observable
     */
    public void update();

}
