package commands;


import exceptions.NoPreviousDirectionsException;
import loader.Direction;


/**
 * The interface LastPositionCommand {@code LastPositionCommand} is used to implement the command pattern.
 * Its goal is to return the last direction Soko was moving to
 */
public interface LastDirectionCommand {

    /**
     * Must be implemented to return the last direction Soko is moving to
     */
    public Direction exec() throws NoPreviousDirectionsException;
}
