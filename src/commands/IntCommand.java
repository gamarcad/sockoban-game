package commands;

import loader.Type;


/**
 * The interface IntCommand {@code IntCommand} is used to implement the command pattern.
 * Its goal is to return an int
 */
public interface IntCommand {

    /**
     * Must be implemented to return an int
     */
    public int exec();
}
