package Model;


import loader.Coordinates;
import loader.Direction;

/**
 * An extends of memorySoko with the position of the block which has been moved
 */
public class memoryBox extends memorySoko {

    /**
     * The position of the box
     */
    private Coordinates box;


    /**
     * Constructor with three parameters
     *
     * @param d         Direction the direction applied
     * @param character The position of the character
     * @param box       The position of the block
     */
    memoryBox(Direction d, Coordinates character, Coordinates box) {
        super(d, character);
        this.box = box;

    }

    /**
     * Constructor by copy
     *
     * @param m the memory we want to copy
     */
    public memoryBox(memoryBox m) {
        super(m.getD(),m.getCharacter(),m.isCurrentState());
        this.box = m.box;

    }

    /**
     * Gives the coordinates of the box
     *
     * @return Coordinates the position of the block
     */
    Coordinates getBox() {
        return box;
    }

    /*
    public void setBox(Coordinates box) {
        this.box = box;
    }*/

    /**
     * Create a string
     *
     * @return String a print of memoryBox
     */
    @Override
    public String toString() {
        return super.toString() +
                "box=" + box + '}';
    }
}
