package loader;

/**
 * The enum {@code Direction} represents a direction.
 * The direction avaiable is North, South, East, West.
 * Each direction is represented by a pair of two integers. This allows to access at a cell for instance in a matrix.
 */
public enum Direction {
    NORTH(0, -1),
    SOUTH(0, 1),
    EAST(1, 0),
    WEST(-1, 0);

    /**
     * The offset used to move the cursor in the matrix.
     */
    private int offset_x, offset_y;

    /**
     * Default constructor.
     *
     * @param offset_x Offset to manipulate x.
     * @param offset_y Offset to manipulate y.
     */
    Direction(int offset_x, int offset_y) {
        this.offset_x = offset_x;
        this.offset_y = offset_y;
    }

    public int getOffset_x() {
        return offset_x;
    }

    public int getOffset_y() {
        return offset_y;
    }
}
