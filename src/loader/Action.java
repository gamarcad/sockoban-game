package loader;

/**
 * This interface is used to executed an action.
 * This mechanism looks like command pattern.
 */
@FunctionalInterface
public interface Action {
    /**
     * Execute an action defined in the expression lambda.
     */
    void action();
}
