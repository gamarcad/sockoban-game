package loader;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Game represents a xsb file.
 * The program uses this class to manipulate more easier a plate.
 */
public class Game implements Iterable<Game.Line> {

    /**
     * Title of the xsb file
     */
    private String title;

    /**
     * Location of the character
     */
    private Character character;

    /**
     * Map of the game
     */
    private Type[][] map;

    /**
     * Height and Width of the map
     */
    private int width, height;

    /**
     * Number of boxs and boxs on goal in the map (at the beginning)
     */
    private int boxs, boxs_on_goal;

    /**
     * Default constructor.
     */
    public Game() {
    }

    /**
     * @param map          Double dimension array of Type objects.
     *                     He contains all characters of the map.
     * @param character    Object which contains position of the character called "Soko"
     * @param boxs         Number of box on the map.
     * @param boxs_on_goal Number of box on their targets.
     * @param title        Title of the map.
     * @param filename     The name of file which contains the strings.
     */
    public Game(Type[][] map, Character character, int boxs, int boxs_on_goal, String title, String filename) {
        // asserts
        assert title != null;
        assert map != null && map.length != 0 && map[0].length != 0;
        assert character != null;
        assert 0 <= boxs && 0 <= boxs_on_goal;

        this.title = title;
        this.map = map;
        this.width = map.length;
        this.height = map[0].length;
        this.character = character;
        this.boxs = boxs;
        this.boxs_on_goal = boxs_on_goal;

        int x = character.getX(), y = character.getY();
        assert 0 <= x && x <= width;
        assert 0 <= y && y <= height;
    }

    /**
     * Check if the coordinates exists or not.
     *
     * @param x Abscissa which will be tested.
     * @param y Ordinate which will be tested.
     * @return {@code True} if the coordinates exists, {@code False} otherwise.
     */
    public boolean exists(int x, int y) {
        return 0 <= x && x < width && 0 <= y && y < height;
    }

    /**
     * Return the Type object contained in the coordinates.
     *
     * @param x Abscissa of the coordinates.
     * @param y Ordinate of the coordinates.
     * @return Type object.
     */
    public Type getType(int x, int y) {
        return map[x][y];
    }

    public String toString() {
        return title;
    }

    /**
     * Position Character accessor used to get the coordinates of the Character.
     *
     * @return Character position.
     */
    public Character getCharacter() {
        return character;
    }

    /**
     * Game's title accessor.
     *
     * @return The title of the game.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Game's map accessor.
     *
     * @return The map of the game.
     */
    public Type[][] getMap() {
        return map;
    }

    /**
     * Width accessor.
     *
     * @return The width of the map.
     */
    public int getWidth() {
        return width;
    }

    /**
     * height accessor.
     *
     * @return The height of the map.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Boxs number accessor.
     *
     * @return The number of box on the map.
     */
    public int getBoxs() {
        return boxs;
    }

    /**
     * Boxs on their target number accessor.
     *
     * @return The number of box on their target, on the map.
     */
    public int getBoxsOnGoal() {
        return boxs_on_goal;
    }

    /**
     * Change the number of box on goal(s)
     *
     * @param boxOnGoal
     */
    public void setBoxOnGoal(int boxOnGoal) { this.boxs_on_goal = boxOnGoal; }

    @Override
    public Iterator<Line> iterator() {
        return new GameIterator();
    }

    /**
     * Iterator of the map.
     * This iterator is used to browse the map.
     * <p>
     * This iterator run across all lines.
     * It's a second iterator called Line which is iterable with Type object.
     */
    public class GameIterator implements Iterator<Line> {
        /**
         * Current index of line.
         */
        private int line_index;

        public GameIterator() {
            this.line_index = 0;
        }

        @Override
        public boolean hasNext() {
            return line_index < height;
        }

        @Override
        public Line next() {
            LinkedList<Type> types = new LinkedList<>();
            for (int i = 0; i < width; ++i)
                types.addLast(map[i][line_index]);
            ++line_index;
            return new Line(types);
        }
    }

    /**
     * The iterator which used to browse the iterator line.
     */
    public class Line implements Iterable<Type> {

        /**
         * The list of Types contained in one line.
         */
        private LinkedList<Type> elements;

        public Line(LinkedList<Type> elements) {
            this.elements = elements;
        }

        @Override
        public Iterator<Type> iterator() {
            return elements.iterator();
        }
    }
}