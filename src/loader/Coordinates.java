package loader;

/**
 * The {@code Coordinates} class represents a coordinate which includes an abscisse and an ordinate.
 */
public class Coordinates {

    /**
     * Abscisse of the coordinate.
     */
    private int x;

    /**
     * Ordinate of the coordinates.
     */
    private int y;

    /**
     * @param x Abscisse of the coordinate.
     * @param y Ordinate of the coordinates.
     */
    public Coordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Set the abscisse of the coordinate.
     *
     * @param x The new abscisse of the coordinate.
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Set the new ordinate of the coordinate.
     *
     * @param y The new ordinate of the coordinate.
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Return the current abcsisse.
     *
     * @return The current abcsisse.
     */
    public int getX() {
        return this.x;
    }

    /**
     * Return the current ordinate.
     *
     * @return The current ordinate.
     */
    public int getY() {
        return this.y;
    }

    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
