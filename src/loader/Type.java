package loader;

/**
 * The {@class Type} enumeration is used to represent a cell of a matrix.
 */
public enum Type {
    EMPTY(' '),
    PLAYER_ON_FLOOR('@', true),
    PLAYER_ON_GOAL('+', true),
    BOX_ON_FLOOR('$', true),
    BOX_ON_GOAL('*', true),
    EMPTY_GOAL('.'),
    WALL('#');

    /**
     * The symbol which is represented by the enumeration.
     */
    private final char symbol;

    /**
     * Define if the represented cell is moveable or not.
     */
    private final boolean moveable;

    /**
     * Default constructor.
     *
     * @param symbol The represented symbol.
     */
    Type(char symbol) {
        this(symbol, false);
    }

    /**
     * @param symbol   The represented symbol.
     * @param moveable {@code true} if the cell is moveable, {@code false} else.
     */
    Type(char symbol, boolean moveable) {
        this.symbol = symbol;
        this.moveable = moveable;
    }

    /**
     * Symbol which is contained by Type object.
     *
     * @return The wrapped symbol.
     */
    public char getSymbol() {
        return symbol;
    }

    /**
     * Check if the character is equals than the character contained in the Type object.
     *
     * @param c The character which will be compared with the wrapped character.
     * @return {@code True} if the character is equals, {@code False} otherwise.
     */
    public boolean equals(char c) {
        return c == symbol;
    }

    /**
     * Check if the Type object is movable in the sockoban game.
     *
     * @return {@code True} if the Type object is movable, {@code False} otherwise.
     */
    public boolean isMoveable() {
        return moveable;
    }

    public String toString() {
        return symbol + "";
    }
}
