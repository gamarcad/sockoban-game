package view.builder.decorator;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

import view.builder.Style;

/**
 * The {@code Decorator} class is used to decor elements of the panel.
 * It allows to keep decoration in just one class.
 */
public class Decorator {

    /**
     * Set dimension of the button.
     * By default, the dimension of the height is 40px and the width is Integer.MAX_VALUE.
     *
     * @param button The button which will be modified.
     */
    public void setDimension(Button button) {
        button.setPrefHeight(40);
        button.setPrefWidth(Integer.MAX_VALUE);
    }

    /**
     * Set decoration of the button.
     *
     * @param style  Style of the button applied to the game.
     * @param button Button which will be decorated.
     */
    public void setGraphics(Style style, Button button) {
        if (style == Style.DISABLE)
            button.setDisable(true);
        setDimension(button);
        setDecoration(style, button);
    }

    /**
     * Set decoration of a label.
     *
     * @param label Label which will be decorated.
     */
    public void setDecoration(Label label) {
        label.setStyle("-fx-background-color: #3c3c3c; -fx-text-fill: #FFFFFF;");
    }

    public void setDecoration(Style style, Button button) {
        setDecoration(style, button, "");
    }

    public void setDecoration(Style style, Button button, String styleSpecifications) {
        setDimension(button);
        switch (style) {
            case ENABLE:
                String themeEnable = "-fx-background-color: #3c3c3c; -fx-text-fill: #FFFFFF;" + styleSpecifications;
                button.setStyle(themeEnable);
                button.setOnMouseEntered(event -> {

                    button.setStyle("-fx-background-color: #202020; -fx-text-fill: #FFFFFF;" + styleSpecifications);
                });
                button.setOnMouseExited(event -> button.setStyle(themeEnable));
                break;

            case DISABLE:
                String themeDisable = "-fx-background-color: #111111; -fx-text-fill: #FFFFFF;" + styleSpecifications;
                button.setStyle(themeDisable);
                break;
        }
    }

    /**
     * Set the decoration of the node.
     *
     * @param node Node which will be decorated.
     */
    public void setDecoration(Node node) {
        setDecoration(node, "");
    }

    /**
     * Set the decoration of the node.
     *
     * @param node                Node which will be decorated.
     * @param styleSpecifications Custom style applied to the node.
     */
    public void setDecoration(Node node, String styleSpecifications) {
        node.setStyle("-fx-background-color: #2d3032;" + styleSpecifications);
    }

    /**
     * Set the decoration of a listView.
     *
     * @param listView ListView which will be decorated.
     */
    public void setDecoration(ListView<?> listView) {
        listView.setStyle("-fx-control-inner-background: #3c3c3c; -fx-text-fill: #ffffff");
    }


}