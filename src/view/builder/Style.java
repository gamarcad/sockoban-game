package view.builder;

/**
 * This enum represents style available for a button.
 */
public enum Style {
    ENABLE,
    DISABLE;
}
